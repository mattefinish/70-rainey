<?php

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="body-content-small">
			<header class="post-header border_line">
				<h1 class="subheader"><?php the_title(); ?></h1>
				<p class="post-date"><?php the_date('j M, Y'); ?></p>
			</header>
			<div class="post-content">
				<?php the_post_thumbnail(); ?>
				<?php the_content(); ?>
				<a class="twitter_share" href="<?php the_permalink(); ?>" text="<?php the_title(); ?>">Share</a>
			</div>
		</div>

	<?php endwhile; ?>

<?php get_footer(); ?>
