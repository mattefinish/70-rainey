<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="icon" type="img/png" href="<?php echo get_template_directory_uri() . '/img/70-rainey-favicon.png'; ?>" />
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>

        <!-- Mobile Header -->

        <div class="mobile_header">
            <a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri() . '/img/70-rainey-logo.svg'; ?>" /></a>
            <div class="mobile_header_icon_container">
                <span class="menu_icon"></span>
                <span class="close_icon"></span>
            </div>
        </div>

        <!-- Mobile Menu -->

        <?php get_template_part('template-parts/mobile_menu'); ?>

        <!-- Main Header -->

        <header class="main-header"> 

            <!-- Logo -->

            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="rainey_logo"><img src="<?php echo get_template_directory_uri() . '/img/70-rainey-logo.svg'; ?>" /></a>

            <!-- Navigation Items -->

            <nav class="main-navigation">
                <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'container' => '' ) ); ?>
            </nav>
            
        </header>

        <div class="main-content"><!-- .main-content closes in footer.php -->

