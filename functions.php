<?php

/*=========================
Sets up theme defaults and registers support for various WordPress features.
 
Note that this function is hooked into the after_setup_theme hook, which
runs before the init hook. The init hook is too late for some features, such
as indicating support for post thumbnails.
========================*/

if ( ! function_exists( 'rainey_setup' ) ) :

function rainey_setup() {
	
	/*==========================================
	LET WORDPRESS MANAGE THE DOCUMENT TITLE
	==========================================*/
	add_theme_support( 'title-tag' );
	
	/*==========================================
	ENABLE SUPPORT FOR POST THUMBNAILS ON POSTS AND PAGES
	==========================================*/
	add_theme_support( 'post-thumbnails' );

	/*==========================================
	SETUP NAVIGATION MENUS
	==========================================*/
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', '_s' ),
	) );

	/*==========================================
	Switch default core markup for search form, comment form, and comments
	to output valid HTML5.
	==========================================*/
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
};

endif;

add_action( 'after_setup_theme', 'rainey_setup' );

/*==========================================
ENQUEUE SCRIPTS AND STYLES
==========================================*/

function rainey_scripts() {
	
	// Default theme style

	wp_enqueue_style( 'rainey-original-styles', get_stylesheet_uri() );

	// Celso new styles

	wp_enqueue_style( 'celso-new-styles', get_template_directory_uri() . '/css/style.min.css' );

	// Wordpress Default Jquery
	
	if (!is_admin()) {
		wp_enqueue_script('jquery');
	}

	// Font Awesome

	wp_enqueue_script('font-awesome', 'https://use.fontawesome.com/bfa5b116b0.js', '', '', true);

	// Flexslider Styles

	wp_enqueue_style('flexslider-css', get_template_directory_uri() . '/css/flexslider/flexslider.min.css');

	wp_enqueue_script( 'flexslider-js', get_template_directory_uri() . '/js/flexslider/jquery.flexslider-min.js', '','', true);

	// Fitvids

	wp_enqueue_script('fitvids', get_template_directory_uri() . '/js/fitvids/fitvids.min.js', '', '', true);

	// Custom Scripts

	wp_enqueue_script('main-scripts', get_template_directory_uri() . '/js/main.js', '', '', true);

	wp_enqueue_script('scripts.min.js', get_template_directory_uri() . '/js/scripts.min.js', '', '', true);

	// Fullpage Scroll

	wp_enqueue_script('fullpage-js', get_template_directory_uri() . '/js/vendor/jquery.fullPage.min.js', '', '', true);

}

add_action( 'wp_enqueue_scripts', 'rainey_scripts' );

/*==========================================
CUSTOM POST TYPES
==========================================*/

/*=== Floor Plnas ===*/

function custom_post_type_floor_plans() {

	$labels = array(
		'name'                => ('Floor Plans'),
		'singular_name'       => ('Floor Plan'),
		'menu_name'           => ('Floor Plans'),
		'parent_item_colon'   => (''),
		'all_items'           => ('All Floor Plans'),
		'view_item'           => ('View Floor Plan'),
		'add_new_item'        => ('Add New Floor Plan'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Floor Plan'),
		'update_item'         => ('Update Floor Plan'),
		'search_items'        => ('Search Floor Plans'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('Floor Plans'),
		'description'         => ('Floor Plans'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'page-attributes', 'excerpt' ),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'floor-plans'),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_icon'  		  => 'dashicons-layout',
	);

	register_post_type( 'floor-plans', $args );

}

add_action( 'init', 'custom_post_type_floor_plans', 0 );

/*=============================================
ACF OPTIONS PAGE
=============================================*/

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
	
}

/*==========================================
LIMIT POST REVISIONS
==========================================*/

function limit_post_revisions( $num, $post ) {
    $num = 3;
    return $num;
}

add_filter( 'wp_revisions_to_keep', 'limit_post_revisions', 10, 2 );

/*=============================================
YOAST
=============================================*/

/*=== Adjust Metabox Priority ===*/

add_filter( 'wpseo_metabox_prio', function() { return 'low';});

/*=============================================
CUSTOM LOGIN SCREEN
=============================================*/

// Change the login logo URL

function my_loginURL() {
    return esc_url( home_url( '/' ) );
}

add_filter('login_headerurl', 'my_loginURL');

// Enqueue the login specific stylesheet for design customizations.

function my_logincustomCSSfile() {
    wp_enqueue_style('login-styles', get_template_directory_uri() . '/css/login.min.css');
}

add_action('login_enqueue_scripts', 'my_logincustomCSSfile');

/*=============================================
DISALLOW FILE EDIT
Remove the ability to edit theme and plugins via the wp-admin.
=============================================*/

function disable_file_editting() {
  define('DISALLOW_FILE_EDIT', TRUE);
}

add_action('init','disable_file_editting');

/*=============================================
SHOW THE CHOICE LABEL INSTEAD OF VALUE FOR GRAVITY FORM ENTRIES
=============================================*/

add_filter( 'gform_entry_field_value', function ( $value, $field, $entry, $form ) {
    $value_fields = array(
        'checkbox',
        'radio',
        'select'
    );

    if ( in_array( $field->get_input_type(), $value_fields ) ) {
        $value = $field->get_value_entry_detail( RGFormsModel::get_lead_field_value( $entry, $field ), '', true, 'text' );
    }

    return $value;
}, 10, 4 );

/*==========================================
GRAVITY FORMS SPLIT COLUMNS
http://www.jordancrown.com/revisited-multi-column-gravity-forms/
==========================================*/

// Create the Gravity Forms column UI element

if(!class_exists('GF_Field_Column') && class_exists('GF_Field')) {
	class GF_Field_Column extends GF_Field {

		public $type = 'column';

		public function get_form_editor_field_title() {
			return esc_attr__('Column Break', 'gravityforms');
		}

		public function is_conditional_logic_supported() {
			return false;
		}

		function get_form_editor_field_settings() {
			return array(
				'column_description',
				'css_class_setting'
			);
		}

		public function get_field_input($form, $value = '', $entry = null) {
			return '';
		}

		public function get_field_content($value, $force_frontend_label, $form) {

			$is_entry_detail = $this->is_entry_detail();
			$is_form_editor = $this->is_form_editor();
			$is_admin = $is_entry_detail || $is_form_editor;

			if($is_admin) {
				$admin_buttons = $this->get_admin_buttons();
				return $admin_buttons.'<label class=\'gfield_label\'>'.$this->get_form_editor_field_title().'</label>{FIELD}<hr>';
			}

			return '';
		}

	}
}

// Register the Column Field

function register_gf_field_column() {
	if(!class_exists('GFForms') || !class_exists('GF_Field_Column')) return;
	GF_Fields::register(new GF_Field_Column());
}

add_action('init', 'register_gf_field_column', 20);

// Manipulate the output html of a column field on a form. This seperates the section before and after a column break.

function filter_gf_field_column_container($field_container, $field, $form, $css_class, $style, $field_content) {
	if(IS_ADMIN) return $field_container;
	if($field['type'] == 'column') {
		$column_index = 2;
		foreach($form['fields'] as $form_field) {
			if($form_field['id'] == $field['id']) break;
			if($form_field['type'] == 'column') $column_index++;
		}
		return '</ul><ul class="'.GFCommon::get_ul_classes($form).' column column_'.$column_index.'">';
	}
	return $field_container;
}
add_filter('gform_field_container', 'filter_gf_field_column_container', 10, 6);

// Add CSS helpers to the groupings so we can control specific column widths.

function filter_gf_multi_column_pre_render($form, $ajax, $field_values) {
	$column_count = 0;
	$prev_page_field = null;
	foreach($form['fields'] as $field) {
		if($field['type'] == 'column') {
			$column_count++;
		} else if($field['type'] == 'page') {
			if($column_count > 0 && empty($prev_page_field)) {
				$form['firstPageCssClass'] = trim((isset($field['firstPageCssClass']) ? $field['firstPageCssClass'] : '').' gform_page_multi_column gform_page_column_count_'.($column_count + 1));
			} else if($column_count > 0) {
				$prev_page_field['cssClass'] = trim((isset($prev_page_field['cssClass']) ? $prev_page_field['cssClass'] : '').' gform_page_multi_column gform_page_column_count_'.($column_count + 1));
			}
			$prev_page_field = $field;
			$column_count = 0;
		}
	}
	if($column_count > 0 && empty($prev_page_field)) {
		$form['cssClass'] = trim((isset($form['cssClass']) ? $form['cssClass'] : '').' gform_multi_column gform_column_count_'.($column_count + 1));
	} else if($column_count > 0) {
		$prev_page_field['cssClass'] = trim((isset($prev_page_field['cssClass']) ? $prev_page_field['cssClass'] : '').' gform_page_multi_column gform_page_column_count_'.($column_count + 1));
	}
	return $form;
}
add_filter('gform_pre_render', 'filter_gf_multi_column_pre_render', 10, 3);

?>