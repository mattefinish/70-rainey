        </div>

        <!-- Footer -->

        <?php get_template_part('template-parts/main_footer'); ?>

        <!-- Mobile Footer -->

        <?php get_template_part('template-parts/mobile_footer'); ?>

        <!-- Lasso Analytics -->

        <?php if(is_page_template('page-templates/inquire.php')): ?>

            <!-- Just Inquire Page -->

            <script type="text/javascript">
                var _ldstJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
                _ldstJsHost += "app.lassocrm.com";
                document.write(unescape("%3Cscript src='" + _ldstJsHost + "/analytics.js' type='text/javascript'%3E%3C/script%3E"));
                </script>
                <script type="text/javascript">
                <!--
                var LassoCRM = LassoCRM || {};
                (function(ns){
                    ns.tracker = new LassoAnalytics('LAS-779561-01');
                })(LassoCRM);
                try {
                    LassoCRM.tracker.setTrackingDomain(_ldstJsHost);
                    LassoCRM.tracker.init();  // initializes the tracker
                    LassoCRM.tracker.track(); // track() records the page visit with the current page title, to record multiple visits call repeatedly.
                } catch(error) {}
                -->
            </script>

            <script type="text/javascript">
                (function(i) {var u =navigator.userAgent;
                var e=/*@cc_on!@*/false; var st = setTimeout;if(/webkit/i.test(u)){st(function(){var dr=document.readyState;
                if(dr=="loaded"||dr=="complete"){i()}else{st(arguments.callee,10);}},10);}
                else if((/mozilla/i.test(u)&&!/(compati)/.test(u)) || (/opera/i.test(u))){
                document.addEventListener("DOMContentLoaded",i,false); } else if(e){     (
                function(){var t=document.createElement("doc:rdy");try{t.doScroll("left");
                i();t=null;}catch(e){st(arguments.callee,0);}})();}else{window.onload=i;}})
                (function() {
                  for(var i = 0; i < document.forms.length; i++) {
                    if(document.forms[i].input_25 != undefined) {
                      document.forms[i].input_25.value = LassoCRM.tracker.readCookie("ut");
                    }
                  }});
            </script>

        <?php else: ?>

            <!-- Universal Tracking -->

            <script type="text/javascript">
                var _ldstJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
                _ldstJsHost += "app.lassocrm.com";
                document.write(unescape("%3Cscript src='" + _ldstJsHost + "/analytics.js' type='text/javascript'%3E%3C/script%3E"));
                </script>
                <script type="text/javascript">
                <!--
                var LassoCRM = LassoCRM || {};
                (function(ns){
                    ns.tracker = new LassoAnalytics('LAS-779561-01');
                })(LassoCRM);
                try {
                    LassoCRM.tracker.setTrackingDomain(_ldstJsHost);
                    LassoCRM.tracker.init();  // initializes the tracker
                    LassoCRM.tracker.track(); // track() records the page visit with the current page title, to record multiple visits call repeatedly.
                } catch(error) {}
                -->
            </script>

        <?php endif; ?>

        <!-- Google Analytics -->

        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-77907060-1','auto');ga('send','pageview');
        </script>
        
    <?php wp_footer(); ?>
    </body>
</html>
