<?php

	/*
	 * Template name: Amenities
	 */

	get_header();
?>

	<?php while ( have_posts() ) : the_post(); ?>

		<div id="fullpage">

			<!-- Section 1 -->

			<div class="section with_fullscreen_images" data-color-scheme="dark">

				<div class="rainey_slider flexslider cream">

					<ul class="slides">

						<?php if(have_rows('amenities_section1_slider')): while(have_rows('amenities_section1_slider')): the_row(); ?>

							<li style="background-image: url('<?php the_sub_field('rainey_slider_image'); ?>');">
								<?php if(get_sub_field('rainey_slider_caption')): ?>
									<span class="image_caption"><?php the_sub_field('rainey_slider_caption'); ?></span>
								<?php endif; ?>
							</li>

						<?php endwhile; endif; ?>

					</ul>

				</div>

			</div>

			<!-- Section 2 -->

			<div class="section" data-color-scheme="light">

				<div class="centered_content_container">
					<div class="centered_content boxed">
						<?php the_field('amenities_section2_content'); ?>
					</div>
				</div>
				
			</div>

			<!-- Section 3 -->

			<div class="section with_fullscreen_images" data-color-scheme="dark">

				<div class="rainey_slider flexslider cream">

					<ul class="slides">

						<?php if(have_rows('amenities_section3_slider')): while(have_rows('amenities_section3_slider')): the_row(); ?>

							<li style="background-image: url('<?php the_sub_field('rainey_slider_image'); ?>');">
								<?php if(get_sub_field('rainey_slider_caption')): ?>
									<span class="image_caption"><?php the_sub_field('rainey_slider_caption'); ?></span>
								<?php endif; ?>
							</li>

						<?php endwhile; endif; ?>

					</ul>

				</div>

			</div>

			<!-- Section 4 -->

			<div class="section with_fullscreen_images" data-color-scheme="light">

				<div class="rainey_slider flexslider navy navy_caption">

					<ul class="slides">

						<?php if(have_rows('amenities_section4_slider')): while(have_rows('amenities_section4_slider')): the_row(); ?>

							<li>
								<div class="centered_slider_image">
									<img src="<?php the_sub_field('rainey_slider_image'); ?>" />
								</div>
								<?php if(get_sub_field('rainey_slider_caption')): ?>
									<span class="image_caption"><?php the_sub_field('rainey_slider_caption'); ?></span>
								<?php endif; ?>
							</li>

						<?php endwhile; endif; ?>

					</ul>

				</div>

			</div>

			<!-- Section 5 -->

			<div class="section" data-color-scheme="light">

				<div class="centered_content_container">
					<div class="centered_content boxed">
						<?php the_field('amenities_section5_content'); ?>
					</div>
				</div>
				
			</div>

			<!-- Section 6 -->

			<div class="section with_fullscreen_images" data-color-scheme="dark">

				<div class="fullscreen_image" style="background-image:url(<?php the_field('amenities_section6_image'); ?>)">
					<?php if(get_field('amenities_section6_caption')): ?>
						<span class="image_caption"><?php the_field('amenities_section6_caption'); ?></span>
					<?php endif; ?>
				</div>

			</div>

		</div>

		<?php get_template_part('template-parts/fullpage_navigation'); ?>

	<?php endwhile; ?>

<?php get_footer(); ?>