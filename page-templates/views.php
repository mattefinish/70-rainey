<?php

	/*
	 * Template name: Views
	 */

	get_header();

	$header_text = get_field('header_text');

?>

  <div class="views_page_container">

  	<div class="views_landing_container" style="background-image:url(<?php the_field('views_landing_image'); ?>);">
  		<div class="views_landing_content">
  			<?php the_field('views_landing_content'); ?>
  		</div>
  	</div>

  	<div class="views_iframe_container">

  		<iframe src="http://panowings.com/folders/70RaineyPhantom4/"></iframe>

  	</div>
  
  </div>

<?php get_footer(); ?>