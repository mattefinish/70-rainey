<?php

	/*
	 * Template name: Homepage
	 */

	get_header();
?>

	<?php while ( have_posts() ) : the_post(); ?>

		<div id="fullpage">

			<div class="section with_fullscreen_images" data-color-scheme="dark">

				<div class="rainey_slider flexslider cream">

					<ul class="slides">

						<?php if(have_rows('homepage_section1_slider')): while(have_rows('homepage_section1_slider')): the_row(); ?>

							<li style="background-image: url('<?php the_sub_field('rainey_slider_image'); ?>');">
								<?php if(get_sub_field('rainey_slider_caption')): ?>
									<span class="image_caption"><?php the_sub_field('rainey_slider_caption'); ?></span>
								<?php endif; ?>
							</li>

						<?php endwhile; endif; ?>

					</ul>

				</div>

			</div>

			<div class="section" data-color-scheme="light">

				<div class="centered_content_container">
					<div class="centered_content boxed">
						<?php the_content(); ?>
					</div>
				</div>
				
			</div>

		</div>

		<?php get_template_part('template-parts/fullpage_navigation'); ?>

	<?php endwhile; ?>

<?php get_footer(); ?>