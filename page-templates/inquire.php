<?php

	/*
	 * Template name: Inquire
	 */

	get_header();
?>

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="inquire_page_container">

			<div class="inquire_content">

				<h1 class="border_line">We Would Love <br />to Hear from You.</h1>
			    <p><a href="tel:<?php the_field('70rainey_phone_number', 'option'); ?>"><?php the_field('70rainey_phone_number', 'option'); ?></a></p>
                <p><a href="mailto:<?php the_field('70rainey_email', 'option'); ?>"><?php the_field('70rainey_email', 'option'); ?></a></p>
				<ul class="social_icons">
                    <li><a href="<?php the_field('70rainey_facebook', 'option'); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="<?php the_field('70rainey_twitter', 'option'); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="<?php the_field('70rainey_instagram', 'option'); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                </ul>

			</div>

			<div class="inquire_form_container">

				<?php gravity_form( 1, false, false, false, '', false ); ?>

			</div>

		</div>

	<?php endwhile; ?>

<?php get_footer(); ?>