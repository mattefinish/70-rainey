<?php

	/*
	 * Template name: News
	 */

	get_header();
?>

<div class="news_page_container">

	<?php

		// Loop through news posts
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		$news_posts_args = array('post_type' => 'post', 'posts_per_page' => 10, 'order' => 'DSC', 'order_by' => 'date', 'paged' => $paged);
		$news_posts_loop = new WP_Query($news_posts_args);
		if ( $news_posts_loop->have_posts() ) : while ( $news_posts_loop->have_posts() ) : $news_posts_loop->the_post();
	?>
		<article class="post">
			<header class="post-header border_line">
				<h2 class="subheader"><?php the_title(); ?></h2>
				<p class="post-date"><?php the_date('j M, Y'); ?></p>
			</header>
			<div class="post-content">
				<div class="featured_image">
					<?php the_post_thumbnail(); ?>
				</div>
				<?php the_content(); ?>
				<ul class="news_social_icons">
					<li>Share:</li> 
					<li class="social_icon_image">
						<a class="fb_share" href="<?php the_permalink(); ?>">
							<img class="default" src="<?php echo get_template_directory_uri() . '/img/icons/facebook-default-navy.svg'; ?>" />
	            			<img class="hover" src="<?php echo get_template_directory_uri() . '/img/icons/facebook-hover.svg'; ?>" />
	            		</a>
	            	</li>
	            	<li class="social_icon_image">
						<a class="twitter_share" href="<?php the_permalink(); ?>" text="<?php the_title(); ?>">
							<img class="default" src="<?php echo get_template_directory_uri() . '/img/icons/twitter-default-navy.svg'; ?>" />
	            			<img class="hover" src="<?php echo get_template_directory_uri() . '/img/icons/twitter-hover.svg'; ?>" />
	            		</a>
	            	</li>
	            	<li class="social_icon_image">
						<a href="mailto:?subject=<?php the_title(); ?>&amp;body=<?php the_permalink(); ?>">
							<img class="default" src="<?php echo get_template_directory_uri() . '/img/icons/mail-default.svg'; ?>" />
	            			<img class="hover" src="<?php echo get_template_directory_uri() . '/img/icons/mail-hover.svg'; ?>" />
	            		</a>
	            	</li>
				</ul>
			</div>
		</article>
		
	<?php endwhile; wp_reset_postdata(); ?>

	<div class="posts_navigation">
		<?php echo get_previous_posts_link('Previous Page'); ?>
		<?php echo get_next_posts_link('Next Page', $news_posts_loop->max_num_pages); ?>
	</div>

	<?php endif; ?>
	
</div>

<?php get_footer(); ?>