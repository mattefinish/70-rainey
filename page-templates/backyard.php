<?php

	/*
	 * Template name: Backyard
	 */

	get_header();
?>

	<?php while ( have_posts() ) : the_post(); ?>

		<div id="fullpage">

			<!-- Section #1: Fullbleed Image -->

			<div class="section" data-color-scheme="dark">

				<div class="fullscreen_image" style="background-image:url(<?php the_field('backyard_section1_image'); ?>)">
					<?php if(get_field('backyard_section1_caption')): ?>
						<span class="image_caption"><?php the_field('backyard_section1_caption'); ?></span>
					<?php endif; ?>
				</div>

			</div>

			<!-- Section #2: Half Screen Slider -->

			<div class="section" data-color-scheme="light">

				<div class="fullscreen_halves switch_order_mobile">

					<div class="half">

						<div class="text_slider flexslider navy">

							<ul class="slides">

								<?php if(have_rows('backyard_section2_slider')): while(have_rows('backyard_section2_slider')): the_row(); ?>

									<li>
										<div class="centered_content_container">
											<div class="centered_content">
												<?php the_sub_field('backyard_section2_slider_content'); ?>
											</div>
										</div>
									</li>

								<?php endwhile; endif; ?>

							</ul>

						</div>

					</div>

					<div class="half with_fullscreen_images">

						<div class="synced_image_slider flexslider cream">

							<ul class="slides">

								<?php if(have_rows('backyard_section2_slider')): while(have_rows('backyard_section2_slider')): the_row(); ?>

									<li style="background-image: url(<?php the_sub_field('backyard_section2_slider_image'); ?>);"></li>

								<?php endwhile; endif; ?>

							</ul>

						</div>

					</div>

				</div>

			</div>

			<!-- Section #3: Video -->

			<div class="section" data-color-scheme="dark">

				<div class="centered_content_container">
					<div class="centered_video_content">
						<h1 class="border_line"><?php the_field('backyard_section3_title'); ?></h1>
						<div class="centered_video">
							<?php the_field('backyard_section3_video_embed'); ?>
						</div>
					</div>
				</div>

			</div>

		</div>

		<?php get_template_part('template-parts/fullpage_navigation'); ?>

	<?php endwhile; ?>

<?php get_footer(); ?>