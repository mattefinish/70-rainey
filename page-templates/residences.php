<?php

	/*
	 * Template name: Residences
	 */

	get_header();
?>

	<?php while ( have_posts() ) : the_post(); ?>

		<div id="fullpage">

			<div class="section with_fullscreen_images" data-color-scheme="dark">

				<div class="fullscreen_image" style="background-image:url(<?php the_field('residences_section1_image'); ?>)">
					<?php if(get_field('residences_section1_caption')): ?>
						<span class="image_caption" style="right: auto; left: 30px;"><?php the_field('residences_section1_caption'); ?></span>
					<?php endif; ?>
				</div>

			</div>

			<div class="section" data-color-scheme="light">
				<div class="centered_content_container">
					<div class="centered_content boxed">
						<?php the_field('residences_section2_content'); ?>
					</div>
				</div>
			</div>

			<div class="section with_fullscreen_images" data-color-scheme="dark">

				<div class="fullscreen_image" style="background-image:url(<?php the_field('residences_section3_image'); ?>)">
					<?php if(get_field('residences_section3_caption')): ?>
						<span class="image_caption"><?php the_field('residences_section3_caption'); ?></span>
					<?php endif; ?>
				</div>

			</div>

			<div class="section" data-color-scheme="light">

				<div class="centered_content_container">
					<div class="centered_content boxed scrollable">
						<?php the_field('residences_section4_content'); ?>
					</div>
				</div>
				
			</div>

		</div>

		<?php get_template_part('template-parts/fullpage_navigation'); ?>

	<?php endwhile; ?>

<?php get_footer(); ?>