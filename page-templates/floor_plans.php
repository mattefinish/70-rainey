<?php

/*
 * Template name: Floor Plans
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="floor_plans_container">

			<div id="floor-plans">
				<?php
					$args = array(
						'post_type' => 'floor-plans',
						'posts_per_page' => -1,
						'orderby' => 'title',
						'order' => 'ASC'
					);
					
					$floor_plans = get_posts( $args );
				?>

				<div class="module-content">
					
					<div class="mobile_floor_plans">
						<div class="mobile_floor_plans_select_container">
							<select id="mobiSelect">
								<?php foreach( $floor_plans as $post ) : setup_postdata( $post ); ?>			
							
									<option value="<?php echo $post->post_name; ?>"><?php echo strtoupper($post->post_name); ?></option>
								
								<?php endforeach; wp_reset_postdata(); ?>
							</select>
						</div>
						<div class="floor-plans">
							<?php foreach( $floor_plans as $post ) : setup_postdata( $post ); ?>
								<div class="floor-plan filter-item <?php echo $post->post_name; ?>">
									<h1><?php the_title(); ?></h1>
									<p>
										<span><?php echo get_field('bed') . ' bed'; ?></span>
										<span>&middot;</span>
										<span><?php echo get_field('bath') . ' bath'; ?></span>
									</p>
									<p>
										<span><?php echo get_field('square_feet') . ' SQ. FT'; ?></span>
										<span>&middot;</span>
										<span><?php echo 'Levels ' . get_field('floor'); ?></span>
									</p>
									<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
									<img  class="floorPlanImage" src="<?php echo $feat_image; ?>">
								</div>
							<?php endforeach; wp_reset_postdata(); ?>
						</div>
				  	</div>

				  	<div class="desktop_floor_plans">

					  	<div class="floor_plans_top">

							<div class="left building-image-container">
								<div class="floorplan-image filter-item initial" style="background-image:url(<?php echo get_template_directory_uri() . '/img/floorplans/70-rainey-exterior.png' ?>);"></div>
								<?php foreach( $floor_plans as $post ) : setup_postdata( $post ); ?>
									<?php $floor_image = get_field('floor_image') ?>
									<div class="floorplan-image filter-item <?php echo $post->post_name; ?>" style="background-image:url(<?php echo $floor_image['url'] ?>);"></div>
								<?php endforeach; wp_reset_postdata(); ?>
							</div>

							<div class="right selector">
								<table>
									<thead>
										<tr>
											<th>Unit</th>
											<th>Floor</th>
											<th>Bed</th>
											<th>Bath</th>
											<th>SQ. FT</th>
											<th>PDF</th>
										</tr>
									</thead>
									<tbody>

										<?php foreach( $floor_plans as $post ) : setup_postdata( $post ); ?>

											<tr data-target="<?php echo $post->post_name; ?>" class="filter">
												<td><?php the_title(); ?></td>
												<td><?php echo get_field('floor'); ?></td>
												<td><?php echo get_field('bed'); ?></td>
												<td><?php echo get_field('bath'); ?></td>
												<td><?php echo get_field('square_feet'); ?></td>
												<td class="download_pdf"><a href="<?php the_field('floor_pdf'); ?>" target="_blank">Download</a></td>
											</tr>

										<?php endforeach; wp_reset_postdata(); ?>

									</tbody>
								</table>
							</div>

						</div>

						<div class="floor-plans floor-plans-lightbox">
							<div class="lightbox_invisible_layer"></div>
							<?php foreach( $floor_plans as $post ) : setup_postdata( $post ); ?>
								<div class="floor-plan filter-item <?php echo $post->post_name; ?>">
									<div class="floor-plans-lightbox-content">
										<h1><?php the_title(); ?></h1>
										<p>
											<span><?php echo get_field('bed') . ' bed'; ?></span>
											<span>&middot;</span>
											<span><?php echo get_field('bath') . ' bath'; ?></span>
										</p>
										<p>
											<span><?php echo get_field('square_feet') . ' SQ. FT'; ?></span>
											<span>&middot;</span>
											<span><?php echo 'Levels ' . get_field('floor'); ?></span>
										</p>
										<p><a href="<?php the_field('floor_pdf'); ?>" target="_blank">Download PDF</a></p>
									</div>
									<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
									<img class="floorPlanImage" src="<?php echo $feat_image; ?>">
									<span class="lightbox_close"></span>
								</div>
							<?php endforeach; wp_reset_postdata(); ?>
						</div>

					</div>

				</div>

			</div>

		</div>

	<?php endwhile; ?>

<?php get_footer(); ?>