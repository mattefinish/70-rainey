<?php

	/*
	 * Template name: Team
	 */

	get_header();
?>

	<?php while ( have_posts() ) : the_post(); ?>

		<div id="fullpage">

			<!-- Section #1: Sackman Enterprises -->

			<div class="section" data-color-scheme="light">

				<div class="fullscreen_halves switch_order_mobile">

					<div class="half">

						<div class="centered_content_container">

							<div class="centered_content">

								<?php the_field('team_section1_content'); ?>

							</div>

						</div>

					</div>

					<div class="half with_fullscreen_images" style="background-image: url(<?php the_field('team_section1_image'); ?>); background-position: left top;"></div>

				</div>

			</div>

			<!-- Section #2: MARKZEFF Design -->

			<div class="section" data-color-scheme="light">

				<div class="fullscreen_halves">

					<div class="half with_fullscreen_images" style="background-image: url(<?php the_field('team_section2_image'); ?>);"></div>

					<div class="half">

						<div class="centered_content_container">

							<div class="centered_content">

								<?php the_field('team_section2_content'); ?>

							</div>

						</div>

					</div>

				</div>

			</div>

			<!-- Section #3: Page -->

			<div class="section" data-color-scheme="light">

				<div class="fullscreen_halves switch_order_mobile">

					<div class="half">

						<div class="centered_content_container">

							<div class="centered_content">

								<?php the_field('team_section3_content'); ?>

							</div>

						</div>

					</div>

					<div class="half with_fullscreen_images" style="background-image: url(<?php the_field('team_section3_image'); ?>);"></div>

				</div>

			</div>

		</div>

		<?php get_template_part('template-parts/fullpage_navigation'); ?>

	<?php endwhile; ?>

<?php get_footer(); ?>