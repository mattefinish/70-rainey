<div class="mobile_menu">

    <!-- Navigation Items -->

    <nav class="mobile-navigation">
        <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'mobile-menu', 'container' => '' ) ); ?>
    </nav>

    <!-- Bottom Content -->

    <div class="mobile_menu_bottom">

        <div class="footer_logos">
            <a href="http://sackman.com/" target="_blank">
                <img class="default" src="<?php echo get_template_directory_uri() . '/img/sackman-default.svg'; ?>" />
            </a>
            <a href="http://www.denpg.com/" target="_blank">
                <img class="default" src="<?php echo get_template_directory_uri() . '/img/den-default.svg'; ?>" />
            </a>
            <img class="eho_logo" src="<?php echo get_template_directory_uri() . '/img/icons/eho.svg'; ?>" /><a class="disclaimer_text" href="<?php echo get_page_link(191); ?>">Disclaimer</a>
        </div>

        <div class="footer_contact">
            <a href=""><?php the_field('70rainey_phone_number', 'option'); ?></a>
            <a href="mailto:<?php the_field('70rainey_email', 'option'); ?>"><?php the_field('70rainey_email', 'option'); ?></a>
        </div>

        <div class="footer_social">
            <ul class="social_icons">
                <li><a href="<?php the_field('70rainey_facebook', 'option'); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="<?php the_field('70rainey_twitter', 'option'); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a href="<?php the_field('70rainey_instagram', 'option'); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
            </ul>
        </div>
        
    </div>

</div>