<?php
// Arrow to navigate the fullpage sections.
?>

<div class="fullpage_nav_arrow cream">
	<img class="down_arrow_cream" src="<?php echo get_template_directory_uri() . '/img/icons/arrow-down-cream.svg'; ?>" />
	<img class="down_arrow_navy" src="<?php echo get_template_directory_uri() . '/img/icons/arrow-down-navy.svg'; ?>" />
</div>