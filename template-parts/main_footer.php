<footer class="main_footer">

    <div class="footer_logos">
        <a href="http://sackman.com/" target="_blank">
            <img class="default" src="<?php echo get_template_directory_uri() . '/img/sackman-default.svg'; ?>" />
            <img class="hover" src="<?php echo get_template_directory_uri() . '/img/sackman-hover.svg'; ?>" />
        </a>
        <a href="http://www.denpg.com/" target="_blank">
            <img class="default" src="<?php echo get_template_directory_uri() . '/img/den-default.svg'; ?>" />
            <img class="hover" src="<?php echo get_template_directory_uri() . '/img/den-hover.svg'; ?>" />
        </a>
        <img class="eho_logo" src="<?php echo get_template_directory_uri() . '/img/icons/eho.svg'; ?>" /><a class="disclaimer_text" href="<?php echo get_page_link(191); ?>">Disclaimer</a>
    </div>

    <?php if(get_field('footer_now_selling', 'option')): ?>

        <div class="footer_now_selling">
            <a href="<?php echo get_page_link(24); ?>">Now Selling</a>
        </div>

    <?php endif; ?>

    <div class="footer_contact">
        <a href="tel:<?php the_field('70rainey_phone_number', 'option'); ?>"><?php the_field('70rainey_phone_number', 'option'); ?></a>
        <a href="mailto:<?php the_field('70rainey_email', 'option'); ?>"><?php the_field('70rainey_email', 'option'); ?></a>
        <ul class="social_icons">
            <li><a href="<?php the_field('70rainey_facebook', 'option'); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="<?php the_field('70rainey_twitter', 'option'); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
            <li><a href="<?php the_field('70rainey_instagram', 'option'); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
        </ul>
    </div>

</footer>