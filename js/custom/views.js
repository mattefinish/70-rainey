(function($) {

	$(document).ready(function() {

	"use strict"; 

		/*================================= 
		VIEWS TRIGGER
		Open views iframe.
		=================================*/

		var viewsIframe = $('.views_iframe_container');

		$('.views_trigger').click(function(){
			viewsIframe.fadeIn('slow');
		});

	});

})(jQuery);