(function($) {

	$(document).ready(function() {

	"use strict"; 

		/*================================= 
		SUBNAVIGATION REVEAL
		=================================*/

		var subMenu = $('ul.sub-menu');

		var showSubNav = function() {
			$('.main-header').addClass('open');
		}

		var hideSubNav = function() {
			$('.main-header').removeClass('open');
		}

		$('.menu li.menu-item-has-children').hover(showSubNav, hideSubNav);

		/*================================= 
		MOBILE MENU TRIGGER
		=================================*/

		var iconContainer = $('.mobile_header_icon_container');

		iconContainer.click(function(){
			if(!$(this).hasClass('open')) {
				$('.mobile_menu').fadeIn();
				$(this).addClass('open');
			}
			else {
				$('.mobile_menu').fadeOut();
				$(this).removeClass('open');
			}
		});

	});

})(jQuery);