(function($) {

	$(document).ready(function() {

	"use strict"; 

		/*================================= 
		FLOOR PLAN POPUP
		=================================*/

		/*== Open Lightbox by clicking on a floor column from the table ===*/

		$('.desktop_floor_plans .filter td').click(function(){
			// Only trigger the lightbox if the table cell isn't for downloading the PDF
			if(!$(this).hasClass('download_pdf')) {
				// Grab the target of the floorplan to show the right floorplan in the lightbox
				var target = $(this).closest($('.filter')).attr('data-target');
				$('html').addClass('lightbox_open');
				$('.floor-plans-lightbox').addClass('visible');
				// Show the specific floorplan in the lightbox
				$('.floor-plans-lightbox .filter-item.' + target).show();
			}
		});

		/*=== Close the popup ===*/

		$('.lightbox_close, .lightbox_invisible_layer').click(function(){
			$('.floor-plans-lightbox').removeClass('visible');
			$('html').removeClass('modal_open');
			$('.floor-plans-lightbox .filter-item').hide();
		});

	});

})(jQuery);