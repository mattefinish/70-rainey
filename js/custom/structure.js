(function($) {

	$(document).ready(function() {

	"use strict"; 

		/*================================= 
		SMOOTH PAGE LOADING
		=================================*/

		setTimeout(function(){
			$('.main-content').addClass('loaded');
		}, 200); 

		/*================================= 
		FULLPAGE JS
		=================================*/

		// Variables

		var navArrow = $('.fullpage_nav_arrow');
		var sectionCount = $('#fullpage .section').length;
		var mq = window.matchMedia( "(min-width: 1024px)" );

		if(mq.matches) {

			// Initialize

			$('#fullpage').fullpage({
				fitToSection: false,
				scrollingSpeed: 600,
				normalScrollElements: '.boxed_content.scrollable, .centered_content.scrollable',
				afterLoad: function(anchorLink, index){

					// Change the navigation arrow color depending on the section we are on.

					var loadedSection = $(this);
					var loadedSectionColor = loadedSection.attr('data-color-scheme');

					if(loadedSectionColor === 'dark') {
						navArrow.removeClass('navy');
						navArrow.addClass('cream');
					}
					
					else {
						navArrow.removeClass('cream');
						navArrow.addClass('navy');
					}

					// On the last section hide the arrow.

		            if(index === sectionCount){
		                navArrow.addClass('hide');
		            }
		            else {
		            	navArrow.removeClass('hide');
		            }
		        }
			});
		}

		// Navigation

		navArrow.click(function(){
			$.fn.fullpage.moveSectionDown();
		});

	});

})(jQuery);