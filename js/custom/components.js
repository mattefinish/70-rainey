(function($) {

	$(document).ready(function() {

	"use strict"; 

		/*================================= 
		FLEXSLIDER
		=================================*/

		// Fullscreen Slider

		$('.rainey_slider').flexslider({
	    	animation: "fade",
	    	controlNav: true,
	    	nextText: '',
	    	prevText: '',
	    	multipleKeyboard: true,
	    	slideshow: true,
	    	slideshowSpeed: 10000
	    });

		var mq = window.matchMedia( "(min-width: 1024px)" );
		var textSlider = $('.text_slider');

		if(mq.matches) {

		    // Synced Sliders

		    $('.synced_image_slider').flexslider({
		    	animation: "fade",
		    	controlNav: true,
		    	directionNav: true,
		    	slideshow: true,
		    	multipleKeyboard: true,
		    	nextText: '',
	    		prevText: '',
	    		slideshowSpeed: 10000,
	    		before: function(slider){
	    			var nextSlidePosition = slider.animatingTo;
	    			textSlider.flexslider(nextSlidePosition);
	    		}
		    });

			$('.text_slider').flexslider({
		    	animation: "fade",
		    	controlNav: true,
		    	multipleKeyboard: true,
		    	directionNav: false,
		    	sync: '.synced_image_slider',
		    	slideshow: true,
	    		slideshowSpeed: 10000
		    });

		}
		else {

			// Synced Sliders Reversed for mobile

			$('.text_slider').flexslider({
		    	animation: "fade",
		    	controlNav: true,
		    	directionNav: false,
		    	smoothHeight: true,
		    	multipleKeyboard: true,
		    	slideshow: true,
	    		slideshowSpeed: 10000
		    });

		    $('.synced_image_slider').flexslider({
		    	animation: "fade",
		    	controlNav: true,
		    	directionNav: true,
		    	multipleKeyboard: true,
		    	nextText: '',
	    		prevText: '',
		    	sync: '.text_slider',
		    	slideshow: true,
	    		slideshowSpeed: 10000
		    });

		}

		/*================================= 
		FITVIDS
		=================================*/

		/*=== Wrap All Iframes with 'video_embed' for responsive videos ===*/

		$('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').wrap("<div class='video_embed'/>");

		/*=== Target div for fitVids ===*/

		$(".video_embed").fitVids();

	});

})(jQuery);