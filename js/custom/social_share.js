(function($) {

	$(document).ready(function() {

	"use strict"; 

		/*================================= 
		SOCIAL SHARE
		=================================*/

		/*== Facebook Share ==*/

		$('.fb_share').click(function(e) {

			e.preventDefault();

			var loc = $(this).attr('href');

			window.open('http://www.facebook.com/sharer.php?u=' + loc,'facebookwindow','height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

		});

		/*== Twitter Share ==*/

		$('.twitter_share').click(function(e){

		    e.preventDefault();

		    var loc = $(this).attr('href');

		    var text = encodeURIComponent($(this).attr('text'));

		    window.open('http://twitter.com/share?url=' + loc + '&text=' + text, 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

		});

	});

})(jQuery);
	