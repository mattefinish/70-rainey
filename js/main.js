(function($) {

  /*==========================
  Mostly old functions from previous developer. Some still power areas like the floor plans.
  Please refer to celso-scripts.js for functions used on the live site.
  ==========================*/

  var is_mobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) );
  var is_tablet = ( /iPad/i.test(navigator.userAgent) );
	
	$('#mobiSelect').on('change', function() {
		$('.floor-plan').hide();  
		$("."+this.value).show(); // or $(this).val()
	});

	$('#views-modal iframe ').css('opacity','0');
  // module open 
  function moduleOpen() {
    $('.module-trigger').on('click', function(){
	
		$('#views-modal iframe').css('opacity','1');

      var targetModule = $(this).attr('data-target')

      $('.module').each(function(){
        if ($(this).attr('id')==targetModule){
          $(this).addClass('visible')
          $('body').addClass('no-scroll')

          if (targetModule ==='interior-specs') {

            
          }
        }

      })

    })
    
  }

  $('.mobi-mod .a1').show();

  // filter buttons
  function filterButtons() {

    $('#interior-specs .filter').on('click', function(){
      targetFilter = $(this).attr('data-target')

      $('.filter.active').removeClass('active');

      $(this).addClass('active');

      $('.filter-item').each(function(){

          if($(this).hasClass(targetFilter)){
            $(this).show()
          } else {
            $(this).hide()
          }
      })
    });

    $('#floor-plans .filter')
      .on('mouseenter', function(){

      // $('.floor-plans').find('.filter-item').hide();

      targetFilter = $(this).attr('data-target')

      $('#floor-plans .filter.active').removeClass('active');

      $(this).addClass('active');

      $('.building-image-container').find('.filter-item').each(function(){
        if($(this).hasClass(targetFilter)){
          $(this).show()
        } else {
          $(this).hide()
        }
      })

    })

  }

  moduleOpen()

  filterButtons()

})( jQuery );