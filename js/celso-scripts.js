var $j = jQuery.noConflict();

$j(document).ready(function() {

	// Call these functions when the html has loaded.

	"use strict"; // Ensure javascript is in strict mode and catches errors.

	/*================================= 
	SMOOTH PAGE LOADING
	=================================*/

	setTimeout(function(){
		$j('.main-content').addClass('loaded');
	}, 200)

	/*================================= 
	LANDING SCREEN
	Landing page animation effects.
	=================================*/

	// Make sure the landing screen only shows once per user. Check first if the user has viewed the Animation on the homepage.

	if(localStorage.viewedAnimation !== 'yes') {

		// Fade sequence for text and images on the screen. Start by fading in the building image and first sentence

		$j('.landing_screen .small_container h1:first-of-type, .landing_screen .small_container img').delay(300).fadeTo(1000, 1, function(){
			// Fade in the second line of text
			$j('.landing_screen .small_container h1:last-of-type').delay(300).fadeTo(1000, 1, function(){
				// Fade out the entire landing screen
				$j('.landing_screen').delay(300).fadeOut(100);
				// Set the localStorage item to yes so that we know this user has already viewed the animation
				localStorage.setItem('viewedAnimation', 'yes');
			});
		});

	}

	/*================================= 
	HOVER EFFECT FOR HOMEPAGE BLOCKS
	=================================*/

	$j('.homepage_hover_block img').hover(function(){
		$j(this).closest('.panel-row-style').find('a.no_underline').toggleClass('underline');
	});

	$j('.rainey_row.homepage_hover_block img').hover(function(){
		$j(this).closest('.homepage_hover_block').find('a.no_underline').toggleClass('underline');
	});

	/*================================= 
	MOBILE NAVIGATION TRIGGER
	=================================*/

	// Open the nav by pressing the hamburger menu icon

	$j('.menu_icon').click(function(){
		$j('.main-header').addClass('active');
	});

	// Close the nav by pressing the close icon
	
	$j('.close_icon').click(function(){
		$j('.main-header').removeClass('active');
	});

	/*================================= 
	CLOSE ICON
	=================================*/

	$j('.close_icon').click(function(){
		$j('.main-header').removeClass('active');
	});

	/*================================= 
	IMAGE SCROLL EFFECT
	Used on Residences Page
	=================================*/

	$j('.main-content').scroll(function(){

		// Set the container variable

		var imageContainer = $j('.layered_image_container');

		// For each image container check whether its near the top of the window. If so then add a class to move the absolute image

		imageContainer.each(function(){
			if($j(this).offset().top < 300) {
				$j(this).addClass('move_image');
			}
		});

	});

});
