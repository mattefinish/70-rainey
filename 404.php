<?php

get_header(); ?>

    <?php while ( have_posts() ) : the_post(); ?>

        <div id="body-content" class="body-content">
            <header class="page-header">
                <h4>Page not found</h4>
            </header>
        </div>

    <?php endwhile; ?>

<?php get_footer(); ?>
